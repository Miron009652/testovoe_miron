<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // 123456789
        $departments = Department::query()->orderBy('id', 'desc')->simplePaginate(10);
        $test = Department::query()->first();
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('departments.create');
    }

    public function edit(Department $department){
        return view('departments.edit', compact('department'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Department $department
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|void
     */
    public function store(DepartmentRequest $request, Department $department){
        $id = Department::query()->where('id', $department['id'])->first();
        if ($id) {
            $department->update($request->only('name'));
            return redirect('/departments')->with('success', __('messages.successUpdateDep'));
        }
        $department = Department::query()->create($request->only('name'));
        $department->save();
        return redirect('/departments')->with('success', __('messages.successCreateDep'));
    }

    public function destroy(Department $department ){
        if ($department->employees()->count('name') > 0) {
            return redirect('/departments')->with('error', __('messages.errorDeleteDep'));
        }
        $department->delete();
        return redirect('/departments')->with('success', __('messages.successDeleteDep'));
    }
}
