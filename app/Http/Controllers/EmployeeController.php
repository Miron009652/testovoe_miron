<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\EmployeeRequest;

class EmployeeController extends Controller
{
    use ValidatesRequests;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $employees = Employee::query()->orderBy('id','desc')->simplePaginate(5);
        return view('employees.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee){
        return view('employees.create', [
            'employee' => $employee,
            'departments' => Department::query()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request, Employee $employee){
        $id = Employee::query()->where('id', $employee['id'])->first();
        if($id){
            $employee->update($request->all());
            $employee->departments()->detach();
            if($request->input('departments')){
                $employee->departments()->sync($request->input('departments'));
            }
            return redirect('/employees')->with('success', __('messages.successUpdateEmp'));
        }
        $employee = Employee::query()->create($request->all());
        if($request->input('departments')) {
            $employee->departments()->sync($request->input('departments'));
        }
        $employee->save();
        return redirect('/employees')->with('success', __('messages.successCreateEmp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee){
        return view('employees.edit', [
            'employee' => $employee,
            'departments' => Department::query()->get()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee){
        $employee->delete();
        return redirect('/employees')->with('success', __('messages.successDeleteEmp'));
    }
}
