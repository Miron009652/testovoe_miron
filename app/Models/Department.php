<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Providers\FormGroupServiceProvider;

class Department extends Model
{
    protected $fillable = [
        'name',
        ];

    public function employees(){
        return $this->belongsToMany(Employee::class);
    }
}

