<?php

return [
    'successCreateDep' => 'Отдел успешно создан!',
    'successUpdateDep' => 'Отдел успешно изменен!',
    'errorDeleteDep'   => 'Отдел не пустой, и не может быть удален!',
    'successDeleteDep' => 'Отдел успешно удален!',
    'successCreateEmp' => 'Сотрудник успешно добавлен!',
    'successUpdateEmp' => 'Сотрудник успешно изменен!',
    'successDeleteEmp' => 'Сотрудник успешно удален!',

];
