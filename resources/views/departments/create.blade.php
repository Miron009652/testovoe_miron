@extends('layouts.app')

@section('title', 'Добавить отдел')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @include('components.errors.errors')
            <form method="POST" action="{{ route('departments.store') }}">
                @csrf
                {{ Form::textInput('name', old('name'), 'Название отдела', ['required'])}}
                <button type="submit" class="btn btn-outline-dark">Добавить отдел</button>
                <div class="btn-group" role="group" aria-label="Third group">
                    <a class="btn btn-outline-dark" href="{{ route('departments.index') }}">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection
