@extends('layouts.app')

@section('title', 'Отделы')

@section('content')
    <a href="{{ route('departments.create') }}" class="btn btn-outline-dark ">Добавить отдел</a>
    @include('components.errors.errorSession')
    <table class="table table-responsive-sm mt-3">
        <thead class="thead-dark">
        <tr>
            <th style='vertical-align:middle' scope="col">Название отдела</th>
            <th style='text-align:center;vertical-align:middle' scope="col">Количество сотрудников</th>
            <th style='text-align:center;vertical-align:middle' scope="col">Максимальная заработная плата</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($departments as $department)
            <tr>
                <th style='vertical-align:middle' scope="row">{{ $department->name }}</th>
                <td style='text-align:center;vertical-align:middle'>{{ $department->employees()->count('name')}}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $department->employees()->max('wages')}}</td>
                <td class="table-buttons">
                    <a href="{{ route('departments.edit', $department) }}" class="btn btn-outline-dark">
                        <i class="fa fa-pencil" aria-hidden="true"> Изменить</i>
                    </a>
                    <form method="POST" action="{{ route('departments.destroy', $department) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-dark">
                            <i class="fa fa-trash"> Удалить</i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $departments->links() }}
@endsection
