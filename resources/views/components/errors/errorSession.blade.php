@if(session()->get('success'))
    <div class="alert alert-success mt-3">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->get('error'))
    <div class="alert alert-danger mt-3">
        {{ session()->get('error') }}
    </div>
@endif
