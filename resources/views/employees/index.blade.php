@extends('layouts.app')

@section('title', 'Сотрудники')

@section('content')
    <a href="{{ route('employees.create') }}" class="btn btn-outline-dark ">Добавить сотрудника</a>
    @include('components.errors.errorSession')
    <table class="table table-responsive-sm mt-3">
        <thead class="thead-dark">
            <tr>
                <th scope="col" style='vertical-align:middle'>Имя</th>
                <th scope="col" style='text-align:center;vertical-align:middle'>Фамилия</th>
                <th scope="col" style='text-align:center;vertical-align:middle'>Отчество</th>
                <th scope="col" style='text-align:center;vertical-align:middle'>Пол</th>
                <th scope="col" style='text-align:center;vertical-align:middle'>Заработная плата</th>
                <th scope="col" style='text-align:center;vertical-align:middle'>Отделы</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
           @foreach($employees as $employee)
             <tr>
                <td style='vertical-align:middle'>{{ $employee->name }}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $employee->surname }}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $employee->patronymic }}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $employee->gender }}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $employee->wages }}</td>
                <td style='text-align:center;vertical-align:middle'>{{ $employee->departments()->pluck('name')->implode(', ')}}</td>
                <td class="table-buttons">
                    <a href="{{ route('employees.edit', $employee) }}" class="btn btn-outline-dark ">
                        <i class="fa fa-pencil " aria-hidden="true"> Изменить</i>
                    </a>
                    <form method="POST" action="{{ route('employees.destroy', $employee) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-dark">
                            <i class="fa fa-trash"> Удалить</i>
                        </button>
                    </form>
                </td>
             </tr>
           @endforeach
        </tbody>
    </table>
    {{ $employees->links() }}
@endsection
