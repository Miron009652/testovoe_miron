@extends('layouts.app')

@section('title', 'Добавить сотрудника')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @include('components.errors.errors')
            <form method="POST" action="{{ route('employees.store') }}">
                @csrf
                {{ Form::textInput('name', old('name'), 'Имя', ['required'])}}
                {{ Form::textInput('surname', old('surname'), 'Фамилия', ['required'])}}
                {{ Form::textInput('patronymic', old('patronymic'), 'Отчество')}}
                <div class="form-group">
                    <label class="control-label">Ваш пол:</label>
                    {{ Form::select('gender', ['Мужской'=>'Мужской', 'Женский'=>'Женский'], null, ['placeholder' => 'Выберите свой пол...']) }}
                </div>
                {{ Form::textInput('wages', old('wages'), 'Заработная плата')}}
                <div class="form-group">
                    <label class="control-label">Отделы:</label>
                    <div class="col-md-3">
                        @foreach($departments as $department)
                            <label class="checkbox style-a">
                                <input type="checkbox" value="{{$department->id}}" name="departments[]">
                                {{ucfirst($department->name)}}
                            </label>
                        @endforeach
                    </div>
                </div>
                <button type="submit" class="btn btn-outline-dark">Добавить сотрудника</button>
                <div class="btn-group" role="group" aria-label="Third group">
                    <a class="btn btn-outline-dark" href="{{ route('employees.index') }}">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection
