@extends('layouts.app')

@section('title', 'Редактировать сотрудника ')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @include('components.errors.errors')
            <form method="POST" action="{{ route('employees.update', $employee )}}">
                @csrf
                @method('PATCH')
                {{ Form::textInput('name', $employee->name, 'Имя',['required'])}}
                {{ Form::textInput('surname', $employee->surname, 'Фамилия', ['required'])}}
                {{ Form::textInput('patronymic', $employee->patronymic, 'Отчество')}}
                <div class="form-group">
                    <div class="row-col-md-7">
                        <label for="employee-gender">Ваш пол:</label>
                        <select name="gender" class="custom-select @error('gender') is-invalid @enderror">
                            <option value=""></option>
                            <option value="Мужской" {{ ($employee->gender === 'Мужской') ? 'selected' : '' }}>Мужской</option>
                            <option value="Женский" {{ ($employee->gender === 'Женский') ? 'selected' : '' }}>Женский</option>
                        </select>
                    </div>
                </div>
                {{ Form::textInput('wages', $employee->wages, 'Заработная плата')}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Отделы</label>
                    <div class="col-sm-10">
                        @foreach($departments as $department)
                            <input type="checkbox" name="departments[]" value="{{$department->id}}"
                                @if($employee->departments->where('id', $department->id)->count())
                                    checked="checked"
                                @endif
                            >
                            <label class="">{{ucfirst($department->name)}}</label>
                            <br>
                        @endforeach
                    </div>
                </div>
                <button type="submit" class="btn btn-outline-dark">Редактировать сотрудника</button>
                <div class="btn-group" role="group" aria-label="Third group">
                    <a class="btn btn-outline-dark" href="{{ route('employees.index') }}">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection
