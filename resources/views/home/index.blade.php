@extends('layouts.app')

@section('title', 'Главная')

@section('content')


    <table class="table table-responsive-sm mt-3">
      <thead class="thead-dark">
        <tr>
           <th scope="col" style='vertical-align:middle'>#</th>
           @foreach($departments as $department)
           <th style='text-align:center;vertical-align:middle' scope="col">{{ $department->name }}</th>
           @endforeach
        </tr>
      </thead>
      <tbody>
      @foreach($employees as $employee)
          <tr>
              <th scope="row">{{ $employee->surname }} {{ $employee->name }}</th>
              @foreach($departments as $department)
                  <td style='text-align:center;vertical-align:middle'>
                      @if($employee->departments->where('id', $department->id)->count())
                          &#10003;
                      @endif
                  </td>
              @endforeach
          </tr>
          @endforeach
      </tbody>
    </table>


@endsection
